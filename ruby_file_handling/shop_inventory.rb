require_relative 'product_details.rb'
require_relative 'user_details.rb'

class ShopInventory
  def initialize
    @array = Array.new
    @product = Product.new
    @user = UserDetails.new
  end

  def check_user
    puts "Who are you?"
    puts "press 1.User   2.shopkeeper"
    user_type = gets
    if user_type.to_i == 1
      puts "1.Buy Product  2.Search Product  3.Display Products"
      select_choice
    else
      puts "1.Buy Product  2.Search Product  3.Display Products  4.Add Product  5.Remove Product"
      select_choice
    end
  end

  def select_choice
    puts "choose one operation"
    choice = gets
    case choice.to_i
      when 1
        puts "buy Product"
        @user.set_details
      when 2
        puts "Search Product"
        puts @product.search_data
      when 3
        puts "Display Product"
        puts  @product.get_all_details
      when 4
        puts "Give Details of Product as Name "
        @product.insert
      when 5
        puts "Remove Product"
        @product.delete_record
      else
        puts "Wrong choice please enter correct"
    end
  end
end

shop = ShopInventory.new
shop.check_user
