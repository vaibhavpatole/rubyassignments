require_relative 'input_taker.rb'
require_relative 'file_manage.rb'

class Product

  def initialize
    @hash ={
      :id => Integer,
      :name => String,
      :comp => String,
      :price => Integer,
      :quantity => Integer
    }
    @updated_data = []
    @input = InputTaker.new(@hash)
    @file=FileManage.new("abc.txt")
  end

  def insert
    @file.insert(@input.set_details)
  end

  def get_all_details
    @file.display_all
  end

  def search_data
    @file.search(@input.get_id)
  end

  def delete_record
    id = @input.get_id
    @data = @file.display_all.split("\n")
    @data.each do |e|
      if(e.start_with?(id))
        next
      else
        @updated_data.push(e.chomp)
      end
    end
    @file.delete(@updated_data)
  end


end


#schngess
