require_relative 'input_taker.rb'
require_relative 'file_manage.rb'

class UserDetails
  def initialize
    @hash ={
      :name => String,
      :cardno => String,
      :cvv => Integer,
    }
    @input = InputTaker.new(@hash)
    @file = FileManage.new("order.txt")
  end

  def set_details
    @file.insert(@input.set_details)

  end

end
