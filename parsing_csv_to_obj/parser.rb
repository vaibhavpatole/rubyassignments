require 'csv'
class Parser
  def initialize
    @array = Array.new
    @file_name = 'locations.csv'
  end
  def get_class_name
    array_1 = @file_name.scan(/([^\.]*)/)
    @class_name = array_1[0].to_s.scan(/[a-z]/).join('')
    #@class_name = File.basename(@file_name,'.csv')
  end
  def file_read
    @table = CSV.parse(File.read(@file_name), headers: false)
    @attr_array = @table[0]
    @attr_array.each do|attr|
      @array.push(attr.to_sym)
    end
  end
  def create_class
    p class_name = @class_name.capitalize
    p array = @array
    @klass = Object.const_set class_name, Struct.new(*array)
    #p @klass.instance_methods(false)
  end
  def get_data
    @table = @table.drop(1)
    @table.each do |el|
      p el
    end
  end
end
parser = Parser.new
parser.get_class_name
parser.file_read
parser.create_class
parser.get_data
