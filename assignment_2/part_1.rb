array_1 = [2, 4, 6, 8, 10]
array_2 = [1, 5, 6, 8, 11, 12]
hash_1 = {a: 'a', b: 'b', c: 'c', d: 'd', e: 'e'}
hash_2 = {x: '10', y: '20', z: '30'}
sum=0

#print string 10 times
10.times(puts "Hello World")
#print number from 30 to 40
Range.new(30,40).to_a
#concat two array into one
new_concat_array = array_1.concat array_2
#find uniq number from concated array
new_concat_array.uniq
#find even numbers from array
new_concat_array.select(&:even?)
#or
new_concat_array.select {|num| num.even?}
#delete variable from array
new_concat_array.delete_if {|var|var>8}
#find the sum of cube
array_1.inject(0) {|sum,x| sum + x*x*x }
puts sum
#find the index of number 8
new_concat_array.find_index(8)
#print array with addition of each element with 5
new_concat_array.map do|element|
  element+5
end
#print array with sum of addition of all elements
value_array = hash_2.values
value_array.each {|element| sum+=element.to_i}
puts sum

key_array = hash_1.keys
#or
[ key_array , value_array ].transpose.to_h