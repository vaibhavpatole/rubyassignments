def calculate(num_1,num_2,&block)
  block.call(num_1,num_2)
end
calculate(4,5){|num1,num2| puts num1+num2}
calculate(4,5){|num1,num2| puts num1-num2}
calculate(4,5){|num1,num2| puts num1*num2}
calculate(4,5){|num1,num2| puts num1/num2}



def compose
  proc_method_1(&Proc.new)
end
def proc_method_1
  value=yield*2
  Proc.new { return (value*value) }.call(value)
end

compose{2}
