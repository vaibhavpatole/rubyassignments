numbers_hash = {
 'user_1'   => [1,2,3,4,5,6,7,9],
 'user_2'   => [1,2,3],
  'user_3'  => [1,2,3,6,7,8],
  'user_4'  => [1,2,3,5,6,8],
  'user_5'  => [1,2,3,10]
}
print "common elements in all users\n"
#convert hole array into one array
new_array = numbers_hash.values.flatten
#find the count of all keys
key_length = numbers_hash.keys.length
#find the unique elements
uniq_no_array = array.find_all{|x| array.count(x)>len-1}.uniq
#remove the common values from all users
value_array = numbers_hash.values.map{|ele|  ele-arr}
#separate key from hash and convert into array
key_array = numbers_hash.keys
new_hash = [key_array,value_array].transpose.to_h

number_hash.delete_if{|key,value| value.sum{|i| i}>26}
numbers_hash.delete_if{|key,value| value.length<=4}

