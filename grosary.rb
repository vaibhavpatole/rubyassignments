class products
  hash_of_items
  function display_all_items
    display_hole_hash
  end
end

class sale_product<products
  product_name
  price
  function update
    update sale_item
  remove
  end
end

class calculator<sale_product
  function get_input
    take all inputs from user
  end
  function count_item
    check item count
    call cal_bill
  end
  function cal_bill
    get price from products
    calculate total price
  end
  function discount
    check discount price from sale_product
    if any then check discount price and item
    check count of that product in cal_bill
    count discount
  end
  function final_bill
    cal_bill - discount
    return bill
  end
end
