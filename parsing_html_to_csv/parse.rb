require 'csv'
class File_handle
  def initialize(file_name)
    @parsed_data = Array.new
    @f = File.open(file_name, 'r')
    @rx_row = />(.*?)</
    @rx_str= /[0-9a-zA-Z,:]/
  end
  def read_file
    @data_lines= @f.readlines
  end
  def parse_line
    @data_lines.map { |e|
      @parsed_data.push(e.scan(@rx_row))
    }
    @parsed_str = @parsed_data.flatten.join(",")
  end
  def convert_type
    str = @parsed_str.scan(@rx_str).join("")
    array = str.split(",")
    @array_1 = array.reject { |c| c.empty? }
  end
  def insert_data
    CSV.open("file.csv", "a+") do |csv|
      @array_1.map.with_index { |e,i|
        csv << [e,@array_1[i+1],@array_1[i+2],@array_1[i+3]]
      }
    end
  end
end

file=File_handle.new('vehicle_reports.html')
file.read_file
file.parse_line
file.convert_type
file.insert_data
